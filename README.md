# Cypress-qa
# Pré-requis
Avoir préalablement installé un environnement NodeJS

Suivre le guide d'installation sur le site de  [Node.js](https://nodejs.org/).


# Installation
```sh
$ git clone https://gitlab.com/wixmove/welcometothejunglecypress.git
$ cd welcometothejunglecypress/
$ npm install 
```

# Configuration
il faut ajouter les cookies suivants "user.id" et "wk_session" avec une session active dans le fichier cypress.env.json

```sh
// cypress.env.json
{
    "url": "https://www.welcomekit.co/dashboard/o/dkxzma3/jobs/2XMOI_yq66e6b",
    "user.id": "NDA2OTU%3D--696da5a6f718377d15027301665c5a9c3057fc8e",
    "wk_session": "501e1a3dd2caff07bc227e4b8d335f76"
}
```

# Lancement du test
```sh
$ npm run cy:run
```