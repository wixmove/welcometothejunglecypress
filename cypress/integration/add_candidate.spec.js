describe('Feature: Ajouter un nouveau candidat', () => {
  before(() => {
    Cypress.Cookies.debug(true)
    cy.setCookie('user.id', Cypress.env('user.id'))
    cy.setCookie('wk_session', Cypress.env('wk_session'))
  })

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('user.id', 'wk_session')
    cy.visit(Cypress.env('url'))
  })

  it('Ajout d\'un nouveau candidat', () => {
    cy.visit('https://www.welcomekit.co/dashboard/o/dkxzma3/jobs/2XMOI_yq66e6b/new-candidate/392777')
    cy.get('input[name=firstname]').type('John')
    cy.get('input[name=lastname]').type('Doe')
    cy.get('input[name=subtitle]').type('CEO')
    cy.get('input[name=email]').type('john.doe@mail.com')
    cy.get('input[name=address]').type('10 rue de la paix')
    cy.get('input[name=city]').type('Paris')
    cy.get('input[name=zip_code]').type('75000')
    cy.get('input[name=media_website]').type('www.johnjohndoe.org')
    cy.get('textarea[name=cover_letter]').type('Motivation letter')
    cy.get('.btn-primary').click()
    cy.get('.card-form-title').should('not.be.visible')
  })

  it('Vérifier la création du candidat sur le dashboard', () => {
    cy.get('.card-thumbnail-name:first').should('contain', 'John Doe')
    cy.get('.card-thumbnail-subtitle:first').should('contain', 'CEO')
  })

  it('Vérifier les informations sur la fiche du candidat', () => {
    cy.get('.card-thumbnail-name:first').click()
    cy.get('.card-header-name').should('contain', 'John Doe')
    cy.get('.card-header-subtitle').should('contain', 'CEO')
    cy.get('.candidate-infos-email').should('contain', 'john.doe@mail.com')
    cy.get('.candidate-infos-address').should('contain', 'Paris')
    cy.get('.card-block-cover-letter').should('contain', 'Motivation letter')
  })
})